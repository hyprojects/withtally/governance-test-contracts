# WithTally Governance Test Contracts

This is a simple governance contract for use with the WithTally Test Harness.

WARNING: These contracts have not been audited and are full of bugs. DO NOT USE IN PRODUCTION ENVIRONMENTS. USE AT YOUR OWN RISK.

## Unit Testing Composition

While OpenZeppelin's governance documentation thoroughly covers how governance works at a programmatic level, it doesn't discuss, in detail, how governance works from an end-user perspective. Therefore, many of the unit tests in the WithTally Governance unit tests cover, in more detail, the outcome a user should expect when interacting with OpenZeppelin/Compound governance.

This section will discuss some of the unit tests which have been included as examples of various governance functionality.

### Delegation

A user needs to fulfil two requirements before casting their vote on a proposal:

1. The user must delegate their tokens to themselves prior to voting.
2. The user must delegate their tokens prior to the proposal's voting delay expiring.

#### should only count votes from users who have delegated their tokens

If tokens are not delegated or are delegated after the voting delay has expired, the user's vote will not be counted.

This unit test provides an example of a user (addr2) casting a vote without first delegating their tokens. This should result in addr2's tokens not being included in the For vote.

Note that when a user casts a vote without delegating their tokens, the `castVote()` method does not revert. Instead, it simply does not count the vote.

#### should only count votes from users who have delegated their tokens before the proposal

If the user delegates their voting power after the proposal's voting delay (in this case there is no delay), their vote will not be counted if they cast a vote. `castVote()` will not issue a revert or any other exception when a user has not delegated their voting power prior to voting.

This unit test attempts to delegate addr2's voting power to addr2 AFTER the proposal's voting delay has expired. This should result in addr2's tokens not being included in the For vote.

### Governor Settings

OpenZeppelin's governance provides a very convenient way of changing voting settings. Some of the settings that can be changed (if GovernorSettings have been implemented) include:

- Voting Delay; The number of blocks before the users can cast a vote,
- Voting Period; How long the proposal is open for voting (in blocks),
- Proposal Threshold; How many tokens a user must hold to submit a proposal (in blocks).

#### should set the voting delay to 1 hour

A simple change of the voting delay from immediately to 1 hour (about 789 blocks).

#### should allow addr2 to delegate voting power to themselves before voting delay expires

The addr2 user should be able to delegate their voting power before the voting delay block number is up. As described in [IGovernor - Voting Delay](https://docs.openzeppelin.com/contracts/4.x/api/governance#IGovernor-votingDelay--).

### OpenZeppelin Governance Voting Conventions (Summary)

- A user can only vote on a proposal when they have delegated their own tokens for voting AND a proposal's voting delay has not yet expired. For example, if the voting delay is set to 789 blocks (roughly 1 hour), the user has about 1 hour from the proposal being submitted to delegate their voting power.
