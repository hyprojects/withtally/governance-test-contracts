import { HardhatUserConfig } from "hardhat/types";
import { task } from "hardhat/config";
import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
import "@typechain/hardhat";
import "hardhat-deploy";
import "hardhat-deploy-ethers";
import "hardhat-contract-sizer";
import "@nomiclabs/hardhat-solhint";
import "hardhat-prettier";
require("dotenv").config();

task("accounts", "Prints the list of accounts", async (_, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

task(
  "blockNumber",
  "Prints the current block number",
  async (_, { ethers }) => {
    await ethers.provider.getBlockNumber().then((blockNumber) => {
    console.log("Current block number: " + blockNumber);
  });
});

const config: HardhatUserConfig = {
  networks: {
    hardhat: {
      accounts: {
        mnemonic: process.env.MNEMONIC
      }
    },
    rinkeby: {
      accounts: {
        mnemonic: process.env.MNEMONIC
      },
      url: process.env.RINKEBY_URL
    }
  },
  solidity: {
    compilers: [
      {
        version: "0.8.4",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      }
    ],
  },
  namedAccounts: {
    deployer: 0,
    tokenOwner: 1,
  },
  mocha: {
    timeout: 60000
  },
  contractSizer: {
    alphaSort: true,
    disambiguatePaths: false,
    runOnCompile: true,
    strict: true
  }
};

export default config;
