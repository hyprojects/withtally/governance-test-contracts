import { waffle, ethers, deployments } from "hardhat";
import { expect } from "chai";
import { BigNumber } from "ethers";

describe("WithTallyToken", function () {
  let token: any;

  let provider: any;

  before(async () => {
    provider = waffle.provider;

    await deployments.fixture(["WithTallyNFTToken"]);
    token = await ethers.getContract("WithTallyNFTToken");

    const [owner, recipient] = await ethers.getSigners();

    await token["safeTransferFrom(address,address,uint256)"](owner.address, recipient.address, 1);
  });

  it("should have a user balance", async function () {
    const [, recipient] = await ethers.getSigners();

    expect(await token.balanceOf(recipient.address)).to.be.equal(1);
  });
});
