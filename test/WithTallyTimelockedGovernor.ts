import { waffle, ethers, deployments } from "hardhat";
import { BigNumber } from "ethers";
import { expect } from "chai";

import type { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

enum VoteType {
  Against,
  For,
  Abstain,
}

enum ProposalState {
  Pending,
  Active,
  Canceled,
  Defeated,
  Succeeded,
  Queued,
  Expired,
  Executed,
}

async function mineNBlocks(provider: any, n: number) {
  for (let index = 0; index < n; index++) {
    await provider.send("evm_mine");
  }
}

describe("WithTallyTimelockedGovernor", () => {
  let governor: any;
  let token: any;
  let timelock: any;
  let provider: any;

  let addr1: SignerWithAddress, addr2: SignerWithAddress;

  const FIVEHUNDREDTHOUSAND = ethers.BigNumber.from("5").mul(
    ethers.BigNumber.from("10").pow("5")
  );

  const BURN = "Burn tokens";

  const ONE_HOUR_TO_BLOCKS = BigNumber.from(Math.ceil(13.14 * 60));

  let amount: BigNumber;

  before(async () => {
    provider = waffle.provider;

    [, addr1, addr2] = await ethers.getSigners();
  });

  beforeEach(async () => {
    const WithTallyTimelockedGovernor = await ethers.getContractFactory("WithTallyTimelockedGovernor");

    const WithTallyTimelock = await ethers.getContractFactory("WithTallyTimelock");

    await deployments.fixture(["WithTallyToken"]);
    token = await ethers.getContract("WithTallyToken");
    await token.deployed();

    timelock = await WithTallyTimelock.deploy(3600, [], []);
    await timelock.deployed();

    governor = await WithTallyTimelockedGovernor.deploy(token.address, timelock.address);

    await governor.deployed();

    const proposerRole = await timelock.PROPOSER_ROLE();
    const executorRole = await timelock.EXECUTOR_ROLE();
    const adminRole = await timelock.TIMELOCK_ADMIN_ROLE();

    await timelock.grantRole(proposerRole, governor.address);
    await timelock.grantRole(executorRole, "0x0000000000000000000000000000000000000000");

    await timelock.revokeRole(adminRole, await provider.getSigner().getAddress());

    await token.transferOwnership(timelock.address);

    amount = ethers.utils.parseEther(FIVEHUNDREDTHOUSAND.toString());

    await token.transfer(addr1.address, amount);
    await token.transfer(addr2.address, amount);
  });

  it("should burn 1 WithTallyToken", async () => {
    const callDatas = [];
    const values = [0];

    await token.transfer(timelock.address, ethers.utils.parseEther("1"));

    callDatas.push(
      token.interface.encodeFunctionData("burn", [ethers.utils.parseEther("1")])
    );

    await token.delegate(await provider.getSigner().getAddress());
    await token.connect(addr1).delegate(addr1.getAddress());

    await governor.propose([token.address], values, callDatas, BURN);

    const descriptionHash = ethers.utils.id(BURN);

    const proposalHash = await governor.hashProposal(
      [token.address],
      values,
      callDatas,
      descriptionHash
    );

    await governor.castVote(proposalHash, VoteType.For);

    await governor.connect(addr1).castVote(proposalHash, VoteType.For);

    await mineNBlocks(provider, 6575);

    await governor.queue([token.address], values, callDatas,       descriptionHash);

    await provider.send("evm_increaseTime", [3600]);
    await mineNBlocks(provider, 1);

    await governor.execute([token.address], values, callDatas, descriptionHash);

    expect(await governor.state(proposalHash)).to.be.equal(
      ProposalState.Executed
    );
  });

  it("should only count votes from users who have delegated their tokens", async () => {
    const targetAddresses = [token.address];
    const callDatas = [];
    const values = [0];

    callDatas.push(
      token.interface.encodeFunctionData("burn", [ethers.utils.parseEther("1")])
    );

    await token.connect(addr1).delegate(addr1.getAddress());

    await governor
      .connect(addr1)
      .propose(targetAddresses, values, callDatas, BURN);

    const descriptionHash = ethers.utils.id(BURN);

    const proposalHash = await governor.hashProposal(
      targetAddresses,
      values,
      callDatas,
      descriptionHash
    );

    await governor.castVote(proposalHash, VoteType.For);

    await governor.connect(addr1).castVote(proposalHash, VoteType.For);

    // This should fail silently and addr2 will not be able to cast a vote.
    await governor.connect(addr2).castVote(proposalHash, VoteType.For);

    const votes = await governor.proposalVotes(proposalHash);

    expect(votes.forVotes).to.be.equal(amount);
  });

  it("should only count votes from users who have delegated their tokens before the proposal", async () => {
    const targetAddresses = [token.address];
    const callDatas = [];
    const values = [0];

    callDatas.push(
      token.interface.encodeFunctionData("burn", [ethers.utils.parseEther("1")])
    );

    await token.connect(addr1).delegate(addr1.getAddress());

    await governor
      .connect(addr1)
      .propose(targetAddresses, values, callDatas, BURN);

    await token.connect(addr2).delegate(addr2.getAddress());

    const descriptionHash = ethers.utils.id(BURN);

    const proposalHash = await governor.hashProposal(
      targetAddresses,
      values,
      callDatas,
      descriptionHash
    );

    await governor.castVote(proposalHash, VoteType.For);

    await governor.connect(addr1).castVote(proposalHash, VoteType.For);

    // This should fail silently and addr2 will not be able to cast a vote.
    await governor.connect(addr2).castVote(proposalHash, VoteType.For);

    const votes = await governor.proposalVotes(proposalHash);

    expect(votes.forVotes).to.be.equal(amount);
  });
});
