import { waffle, ethers, deployments } from "hardhat";
import { BigNumber } from "ethers";
import { expect } from "chai";

import type { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";

enum VoteType {
  Against,
  For,
  Abstain,
}

enum ProposalState {
  Pending,
  Active,
  Canceled,
  Defeated,
  Succeeded,
  Queued,
  Expired,
  Executed,
}

async function mineNBlocks(provider: any, n: number) {
  for (let index = 0; index < n; index++) {
    await provider.send("evm_mine");
  }
}

describe("WithTallyNFTTestGovernor", function () {
  let governor: any;
  let token: any;

  let provider: any;

  let addr1: SignerWithAddress, addr2: SignerWithAddress;

  const MINT = "Mint tokens";

  const ONE_HOUR_TO_BLOCKS = BigNumber.from(Math.ceil(13.14 * 60));

  before(async () => {
    provider = waffle.provider;

    [, addr1, addr2] = await ethers.getSigners();
  });

  beforeEach(async () => {
    const WithTallyTestGovernor = await ethers.getContractFactory("WithTallyTestGovernor");

    await deployments.fixture(["WithTallyNFTToken"]);
    token = await ethers.getContract("WithTallyNFTToken");

    await token.deployed();

    governor = await WithTallyTestGovernor.deploy(token.address, "1");

    await governor.deployed();

    await token.transferOwnership(governor.address);

    await token["safeTransferFrom(address,address,uint256)"](provider.getSigner().getAddress(), addr1.address, 1);
  });

  it("should mint 1 WithTallyNFTToken", async () => {
    const targetAddresses = [];
    const callDatas = [];
    const values = [0];

    targetAddresses.push(token.address);

    callDatas.push(
      token.interface.encodeFunctionData("safeMint", [addr1.address])
    );

    await token.connect(addr1).delegate(addr1.getAddress());

    await governor
      .connect(addr1)
      .propose(targetAddresses, values, callDatas, MINT);

    const descriptionHash = ethers.utils.id(MINT);

    const proposalHash = await governor.hashProposal(
      [token.address],
      values,
      callDatas,
      descriptionHash
    );

    await governor.castVote(proposalHash, VoteType.For);

    await governor.connect(addr1).castVote(proposalHash, VoteType.For);

    await mineNBlocks(provider, 6575);

    await governor.execute([token.address], values, callDatas, descriptionHash);

    expect(await governor.state(proposalHash)).to.be.equal(
      ProposalState.Executed
    );
  });

  it("should only count votes from users who have delegated their tokens", async () => {
    const targetAddresses = [token.address];
    const callDatas = [];
    const values = [0];

    callDatas.push(
      token.interface.encodeFunctionData("safeMint", [addr1.address])
    );

    await token.connect(addr1).delegate(addr1.getAddress());

    await governor
      .connect(addr1)
      .propose(targetAddresses, values, callDatas, MINT);

    const descriptionHash = ethers.utils.id(MINT);

    const proposalHash = await governor.hashProposal(
      targetAddresses,
      values,
      callDatas,
      descriptionHash
    );

    await governor.castVote(proposalHash, VoteType.For);

    await governor.connect(addr1).castVote(proposalHash, VoteType.For);

    // This should fail silently and addr2 will not be able to cast a vote.
    await governor.connect(addr2).castVote(proposalHash, VoteType.For);

    const votes = await governor.proposalVotes(proposalHash);

    expect(votes.forVotes).to.be.equal(1);
  });

  it("should only count votes from users who have delegated their tokens before the proposal", async () => {
    const targetAddresses = [token.address];
    const callDatas = [];
    const values = [0];

    callDatas.push(
      token.interface.encodeFunctionData("safeMint", [addr1.address])
    );

    await token.connect(addr1).delegate(addr1.getAddress());

    await governor
      .connect(addr1)
      .propose(targetAddresses, values, callDatas, MINT);

    await token.connect(addr2).delegate(addr2.getAddress());

    const descriptionHash = ethers.utils.id(MINT);

    const proposalHash = await governor.hashProposal(
      targetAddresses,
      values,
      callDatas,
      descriptionHash
    );

    await governor.castVote(proposalHash, VoteType.For);

    await governor.connect(addr1).castVote(proposalHash, VoteType.For);

    // This should fail silently and addr2 will not be able to cast a vote.
    await governor.connect(addr2).castVote(proposalHash, VoteType.For);

    const votes = await governor.proposalVotes(proposalHash);

    expect(votes.forVotes).to.be.equal(1);
  });

  describe("Governor Settings", async () => {
    describe("Voting Delay", async() => {
      beforeEach(async () => {
        const targetAddresses = [governor.address];
        const callDatas = [];
        const values = [0];
        const description = "Set Voting Delay";

        callDatas.push(
          governor.interface.encodeFunctionData("setVotingDelay", [
            ONE_HOUR_TO_BLOCKS,
          ])
        );

        await token.connect(addr1).delegate(addr1.getAddress());

        await governor
          .connect(addr1)
          .propose(targetAddresses, values, callDatas, description);

        const descriptionHash = ethers.utils.id(description);

        const proposalHash = await governor.hashProposal(
          targetAddresses,
          values,
          callDatas,
          descriptionHash
        );

        await governor.connect(addr1).castVote(proposalHash, VoteType.For);

        await mineNBlocks(provider, 6575);

        await governor.execute(
          targetAddresses,
          values,
          callDatas,
          descriptionHash
        );
      });

      it("should set the voting delay to 1 hour", async () => {
        expect(await governor.votingDelay()).to.be.equal(ONE_HOUR_TO_BLOCKS);
      });

      it("should not allow any votes for 1 hour after the proposal", async () => {
        const targetAddresses = [token.address];
        const callDatas = [];
        const values = [0];

        callDatas.push(
          token.interface.encodeFunctionData("safeMint", [addr1.address])
        );

        await token.connect(addr1).delegate(addr1.getAddress());

        await governor
          .connect(addr1)
          .propose(targetAddresses, values, callDatas, MINT);

        const descriptionHash = ethers.utils.id(MINT);

        const proposalHash = await governor.hashProposal(
          targetAddresses,
          values,
          callDatas,
          descriptionHash
        );

        await expect(
          governor.connect(addr1).castVote(proposalHash, VoteType.For)
        ).to.be.revertedWith("Governor: vote not currently active");
      });

      it("should allow addr2 to delegate voting power to themselves before voting delay expires", async () => {
        const targetAddresses = [token.address];
        const callDatas = [];
        const values = [0];

        callDatas.push(
          token.interface.encodeFunctionData("safeMint", [addr1.address])
        );


        await governor
          .connect(addr1)
          .propose(targetAddresses, values, callDatas, MINT);

        await mineNBlocks(provider, 1);

        await token.connect(addr2).delegate(addr2.getAddress());

        const descriptionHash = ethers.utils.id(MINT);

        const proposalHash = await governor.hashProposal(
          targetAddresses,
          values,
          callDatas,
          descriptionHash
        );

        await mineNBlocks(provider, ONE_HOUR_TO_BLOCKS.toNumber());

        // should still not be allowed cast vote because addr2 delegated after proposal.
        governor.connect(addr2).castVote(proposalHash, VoteType.For);

        const votes = await governor.proposalVotes(proposalHash);

        expect(votes.forVotes).to.be.equal(0);

        await mineNBlocks(provider, 1);
      });
    });

    describe("Proposal Threshold", () => {
      beforeEach(async () => {
        const targetAddresses = [token.address, governor.address];
        const callDatas = [];
        const values = [0, 0];
        const description = "Set Proposal Threshold";

        callDatas.push(
          token.interface.encodeFunctionData("safeMint", [addr1.address])
        );

        callDatas.push(
          governor.interface.encodeFunctionData("setProposalThreshold", [2]));

        await token.connect(addr1).delegate(addr1.getAddress());

        await governor
          .connect(addr1)
          .propose(targetAddresses, values, callDatas, description);

        const descriptionHash = ethers.utils.id(description);

        const proposalHash = await governor.hashProposal(
          targetAddresses,
          values,
          callDatas,
          descriptionHash
        );

        await governor.connect(addr1).castVote(proposalHash, VoteType.For);

        await mineNBlocks(provider, 6575);

        await governor.execute(
          targetAddresses,
          values,
          callDatas,
          descriptionHash
        );
      });

      it("should change the proposal threshold to 2 NFT tokens", async() => {
        expect(await governor.proposalThreshold()).to.be.equal(2);
      });

      it("should be able to propose with 2 NFT tokens", async() => {
        const targetAddresses = [];
        const callDatas = [];
        const values = [0];

        targetAddresses.push(token.address);

        callDatas.push(
          token.interface.encodeFunctionData("safeMint", [addr1.address])
        );

        await token.connect(addr1).delegate(addr1.getAddress());

        await governor
          .connect(addr1)
          .propose(targetAddresses, values, callDatas, "Mint Another Token");

        const descriptionHash = ethers.utils.id("Mint Another Token");

        const proposalHash = await governor.hashProposal(
          [token.address],
          values,
          callDatas,
          descriptionHash
        );

        await governor.castVote(proposalHash, VoteType.For);

        await governor.connect(addr1).castVote(proposalHash, VoteType.For);

        await mineNBlocks(provider, 6575);

        // await governor.execute([token.address], values, callDatas, descriptionHash);

        /* expect(await governor.state(proposalHash)).to.be.equal(
          ProposalState.Executed
        );*/
      });
    });
  });
});
