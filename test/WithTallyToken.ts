import { waffle, ethers, deployments } from "hardhat";
import { expect } from "chai";
import { BigNumber } from "ethers";

describe("WithTallyToken", function () {
  let token: any;

  let provider: any;

  const FIVEHUNDREDTHOUSAND = ethers.utils.parseUnits("5", 5);

  let amount: BigNumber;

  before(async () => {
    provider = waffle.provider;

    await deployments.fixture(["WithTallyToken"]);
    token = await ethers.getContract("WithTallyToken");

    const [, addr1] = await ethers.getSigners();

    amount = ethers.utils.parseEther(FIVEHUNDREDTHOUSAND.toString());

    await token.transfer(addr1.address, amount);
  });

  it("should have a user balance", async function () {
    const totalSupply = await token.totalSupply();

    expect(totalSupply.sub(amount)).to.be.equal(
      await token.balanceOf(provider.getSigner().getAddress())
    );
  });
});
